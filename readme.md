# Machine Learning in behavior-based architecture
This repository contains LaTeX files and materials I used for my master thesis. The project is based on ROS (Robot Operating System). It was written mainly in Python but there is also a Gazebo plugin written in C++. The project uses OpenCV, Scikit-learn and PyQt.

## Links
* [source code](https://bitbucket.org/jacek143/magellan_packages/)
* [videos](https://www.youtube.com/watch?v=oDa3nAZ8Sp0&list=PLRMF3xu4xb56F1-Vqrjuh-NZ2kzh8XTp9&index=2/)