# -*- coding: utf-8 -*-
"""
Created on Tue May 30 22:43:35 2017

@author: jacek
"""

import numpy as np
import matplotlib.pyplot as plt

plt.figure(0)
rewards = np.loadtxt('rewards.txt')
plt.plot(rewards)
plt.ylabel('reward value')
plt.xlabel('episode number')
plt.savefig('../../img/ex2_rewards.png', bbox_inches='tight')

plt.figure(1)
step  = 50
avr_rewards = np.array([])
for i in range(len(rewards)/step):
  avr_rewards = np.append(avr_rewards,np.average(rewards[i*step:(i+1)*step]))
plt.gca().bar(np.arange(len(avr_rewards))*step, avr_rewards, step)
plt.xlabel('episode number')
plt.ylabel('average reward value')
plt.savefig('../../img/ex2_avr_rewards.png', bbox_inches='tight')

plt.figure(2)
state_action_values = np.loadtxt('state_action_values.txt')
S = state_action_values.shape[0]
A = state_action_values.shape[1]
plt.imshow(state_action_values, interpolation='nearest')
plt.colorbar()
plt.yticks(np.arange(0, S, 1))
plt.ylabel('state nr')
plt.xticks(np.arange(0, A, 1))
plt.xlabel('action nr')
plt.savefig('../../img/ex2_state_action_values.png', bbox_inches='tight')
