#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
=========================================================
PCA example with Iris Data-set
=========================================================

Principal Component Analysis applied to the Iris dataset.

See `here <https://en.wikipedia.org/wiki/Iris_flower_data_set>`_ for more
information on this dataset.

"""
print(__doc__)


# Code source: Gaël Varoquaux
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from sklearn import decomposition
from sklearn import preprocessing

loaded = np.loadtxt('line_detection.txt')
y = loaded[:,0].astype(int)
X = loaded[:,1:]

scaler = preprocessing.StandardScaler()
X = scaler.fit_transform(X)

pca = decomposition.PCA(n_components=2)
pca.fit(X)
X = pca.transform(X)

not_line = plt.scatter(X[:, 0][y==0], X[:, 1][y==0], marker='o', color='b')
line = plt.scatter(X[:, 0][y==1], X[:, 1][y==1], marker='x', color='r')
plt.legend((not_line,line),('not line','line'),scatterpoints=1)

plt.show()
